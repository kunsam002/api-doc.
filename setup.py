from flask import Flask
import config
from flask_sqlalchemy import SQLAlchemy


def create_app(application_name, config_name, **kwargs):
	# factory function for setting up application

	app = Flask(application_name)

	app.config.from_object(config_name)

	app.db = SQLAlchemy(app)

	return app

def initialize_blueprints(app, *blueprints):

	for arg in blueprints:
		app.register_blueprint(arg)

	return app