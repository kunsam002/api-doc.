from wtforms import *
from flask.ext.wtf import html5
from wtforms.validators import *

# creating the forms for the different tables in the database


class SignUpForm(Form):
    name = StringField('Enter name here',validators=[DataRequired()])
    password = PasswordField('Enter Password Here', validators=[DataRequired()])
    retype_password = PasswordField('Retype Password Here', validators=[DataRequired()])
    email = html5.EmailField('email', validators=[DataRequired()])

    def validate_retype_password(form, field):
        if field.data != form.password.data:
            raise ValidationError("Passwords don't match")


class LoginForm(Form):
    email = html5.EmailField('email', validators=[DataRequired()])
    password = PasswordField('Enter Password Here', validators=[DataRequired()])


class AppForm(Form):
    name = StringField('Enter name here',validators=[DataRequired()])
    handle = StringField('Enter a unique name here', validators=[DataRequired()])
    description = TextAreaField('Add a description', validators=[DataRequired()])


class NavigationForm(AppForm):
    app_id = SelectField('Select AppID', coerce=int)


class ProgrammingLanguageForm(Form):
    name = StringField('Enter name here',validators=[DataRequired()])
    handle = StringField('Enter a unique name here', validators=[DataRequired()])


class DocumentForm(Form):
    name = StringField('Enter name here',validators=[DataRequired()])
    description = TextAreaField('Add document details', validators=[DataRequired()])
    app_id = SelectField('Select App id', coerce=int)
    navigation_id = SelectField('Select navigation id', coerce=int)


class ProgrammingCodeForm(Form):
    description = TextAreaField('Add document details', validators=[DataRequired()])
    handle = StringField('Enter a unique name here', validators=[DataRequired()])
    programming_language_id = SelectField('Select Programming Language', coerce=int)
    app_id = SelectField('Select App id', coerce=int)
    document_id = SelectField('Select Document id', coerce=int)