from container.models import *
from populate import *

# creating admin user
# updating users
# deleting users
# encrypting password


def check_password(user_id, password, **kwargs):
    # check if password matches that of the user object
    user = User.query.get(user_id)

    if not user:
        raise Exception('User not found')

    hashed_password = bcrypt.generate_password_hash(password)

    if hashed_password != user.hashed_password:
        raise Exception('Incorrect pasword')

    return user


def hash_password(password, **kwargs):
    # encrypt password to generate a hash using bcrypt
    hashed_password = bcrypt.generate_password_hash(password)

    return hashed_password


def create_user(**kwargs):
    # creating an instance of the user model
    user = User()
    password = hash_password(kwargs.get('password'))
    kwargs['hashed_password'] = password

    user = populate_object(user, **kwargs)

    db.session.add(user)

    try:
        db.session.commit()
    except:
        raise Exception("User not created")


def update_user(obj_id, **kwargs):

    user = User.query.get(obj_id)
    if user is None:
        raise Exception("User is not present")
    user = populate_object(user, **kwargs)

    db.session.add(user)
    try:
        db.session.commit()
    except:
        raise Exception("User not updated")


def delete_user(obj_id,**kwargs):

    user = User.query.get(obj_id)
    if user is None:
        raise Exception("User is not present")

    db.session.delete(user)

    try:
        db.session.commit()
    except:
        raise Exception("User is not deleted")

# def encrypt_password()