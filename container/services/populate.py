from container.models import *


def populate_object(obj, ignored=[], **kwargs):
    # populate an object obj with data from a dictionary kwargs while ignoring data in ignored list

    data = kwargs
    for element in data.keys():
        if element in ignored:
            data.pop(element)

    for name, value in data.items():
        if hasattr(obj, name):
            setattr(obj, name, value)

    return obj

