# creating document
# editing document
# deleting document
# creating child document

from container.models import *
from populate import *


def create_document(obj_id, **kwargs):
    # creating an instance of the document model
    if obj_id:
        parent = Document.query.get(obj_id)
        if parent:
            kwargs["parent_id"] = parent.id

    document = Document()
    document = populate_object(document, **kwargs)
    db.session.add(document)

    try:
        db.session.commit()
    except:
        raise Exception("Document not created")


def update_document(obj_id, **kwargs):

    document = Document.query.get(obj_id)
    if document is None:
        raise Exception("Document does not exist")
    document = populate_object(document, **kwargs)

    db.session.add(document)

    try:
        db.session.commit()
    except:
        raise Exception("Document not updated")


def delete_document(obj_id, **kwargs):

    document = Document.query.get(obj_id)
    if document is None:
        raise Exception("Document does not exist")
    db.session.delete(document)

    try:
        db.session.commit()
    except:
        raise Exception("Document not deleted")


