# creating app
# editing app
# deleting app

from container.models import *
from populate import *


def create_app(**kwargs):
    # creating an instance of the app model

    application = App()

    application = populate_object(application,**kwargs)

    db.session.add(application)

    try:
        db.session.commit()
    except:
        raise Exception("Application not created")


def update_app(obj_id, **kwargs):

    application = App.query.get(obj_id)
    if application is None:
        raise Exception("Application does not exist")
    application = populate_object(application, **kwargs)

    db.session.add(application)

    try:
        db.session.commit()
    except:
        raise Exception("Application not updated")


def delete_app(obj_id, **kwargs):

    application = App.query.get(obj_id)
    if application is None:
        raise Exception("Application does not exist")

    db.session.delete(application)

    try:
        db.session.commit()
    except:
        raise Exception("Application not deleted")

# creating nav
# editing nav
# deleting nav

def create_navigation(**kwargs):
    # creating an instance of the navigation model

    navigation = Navigation()

    navigation = populate_object(navigation, **kwargs)

    db.session.add(navigation)

    try:
        db.session.commit()
    except:
        raise Exception("Navigation not created")


def update_navigation(obj_id, **kwargs):

    navigation = Navigation.query.get(obj_id)

    if navigation is None:
        raise Exception("Navigation does not exist")

    navigation = populate_object(navigation, **kwargs)

    db.session.add(navigation)

    try:
        db.session.commit()
    except:
        raise Exception("Navigation not updated")


def delete_navigation(obj_id, **kwargs):

    navigation = Navigation.query.get(obj_id)

    if navigation is None:
        raise Exception("Navigation does not exist")

    db.session.delete(navigation)

    try:
        db.session.commit()
    except:
        raise Exception("Navigation not deleted")

# add services for progamming languages and for programming codes

def create_programming_language(**kwargs):
    # creating an instance of the navigation model

    programming_language = ProgrammingLanguage()

    programming_language = populate_object(programming_language, **kwargs)

    db.session.add(programming_language)

    try:
        db.session.commit()
    except:
        raise Exception("programming language not created")


def update_programming_language(obj_id, **kwargs):

    programming_language = ProgrammingLanguage.query.get(obj_id)

    if programming_language is None:
        raise Exception("programming language does not exist")

    programming_language = populate_object(programming_language, **kwargs)

    db.session.add(programming_language)

    try:
        db.session.commit()
    except:
        raise Exception("programming language not updated")


def delete_programming_language(obj_id, **kwargs):

    programming_language = ProgrammingLanguage.query.get(obj_id)

    if programming_language is None:
        raise Exception("programming language does not exist")

    db.session.delete(programming_language)

    try:
        db.session.commit()
    except:
        raise Exception("programming language not deleted")


#for prorgamming codes


def create_programming_code(**kwargs):
    # creating an instance of the navigation model

    programming_code = ProgrammingCode()

    programming_code = populate_object(programming_code, **kwargs)

    db.session.add(programming_code)

    try:
        db.session.commit()
    except:
        raise Exception("programming code not created")


def update_programming_code(obj_id, **kwargs):

    programming_code = ProgrammingCode.query.get(obj_id)

    if programming_code is None:
        raise Exception("programming code does not exist")

    programming_code = populate_object(programming_code, **kwargs)

    db.session.add(programming_code)

    try:
        db.session.commit()
    except:
        raise Exception("programming code not updated")


def delete_programming_code(obj_id, **kwargs):

    programming_code = ProgrammingCode.query.get(obj_id)

    if programming_code is None:
        raise Exception("programming code does not exist")

    db.session.delete(programming_code)

    try:
        db.session.commit()
    except:
        raise Exception("programming code not deleted")