from container import app,db
from flask_sqlalchemy import SQLAlchemy
from flask_bcrypt import Bcrypt
db = SQLAlchemy(app)

bcrypt = Bcrypt(app)


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(150), nullable=False)
    password = db.Column(db.Text, nullable=False)
    hashed_password = db.Column(db.Text, nullable=False)
    email = db.Column(db.String(150), nullable=False)

    # def __init__(self,name,password,email,hashed_password):
    #     self.name = name
    #     self.password = password
    #     self.email = email
    #     self.hashed_password = hashed_password

    def __repr__(self):
        return '<User %r>' % self.name

    # @property
    # def hash_password(self):
    #     hashed_password = bcrypt.generate_password_hash(self.password)
    #
    #     return hashed_password


class App(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50),nullable=False)
    handle = db.Column(db.String(50), unique=True,nullable=False )
    description = db.Column(db.Text, nullable=False)

    navigations = db.relationship('Navigation', backref='app')
    documents = db.relationship('Document', backref='app')
    programming_codes = db.relationship('ProgrammingCode', backref='app')

    def __init__(self, name, handle, description):
        self.name = name
        self.handle = handle
        self.description = description

    def __repr__(self):
        return '<App %r>' % self.name


class Navigation(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50), nullable=False)
    handle = db.Column(db.Text, unique=True, nullable=False)
    description = db.Column(db.Text, nullable=False)
    app_id = db.Column(db.Integer, db.ForeignKey('app.id'), nullable=False)

    documents = db.relationship('Document', backref='navigation')

    def __init__(self, name, handle,description,app_id):
        self.name = name
        self.handle = handle
        self.description = description
        self.app_id = app_id

    def __repr__(self):
        return '<%r>' % self.name


class ProgrammingLanguage(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50), nullable=False)
    handle = db.Column(db.Text, unique=True, nullable=False)

    programming_codes = db.relationship('ProgrammingCode', backref='programming_language')

    def __init__(self,name,handle):
        self.name = name
        self.handle = handle

    def __repr__(self):
        return 'ProgrammingLanguage %r' % self.handle


class Document(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    parent_id = db.Column(db.Integer, nullable=False)
    name = db.Column(db.Text ,nullable=False)
    description = db.Column(db.Text, nullable=False)
    app_id = db.Column(db.Integer,db.ForeignKey('app.id'), nullable=False)
    navigation_id = db.Column(db.Integer,db.ForeignKey('navigation.id'),  nullable=False)
    programming_codes = db.relationship('ProgrammingCode', backref='document')

    def __init__(self, name, description, app_id, navigation_id):
        self.name = name
        self.description = description
        self.app_id = app_id
        self.navigation_id = navigation_id

    def __repr__(self):
        return '<Document %r>' % self.name


class ProgrammingCode(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    description = db.Column(db.Text, nullable=False)
    handle = db.Column(db.Text, unique=True, nullable=False)
    programming_language_id = db.Column(db.Integer, db.ForeignKey('programming_language.id'), nullable=False)
    app_id = db.Column(db.Integer, db.ForeignKey('app.id'), nullable=False)
    document_id = db.Column(db.Integer, db.ForeignKey('document.id'), nullable=False)

    def __init__(self, description,handle ,programming_language_id,app_id,navigation_id,document_id):
        self.description = description
        self.handle = handle
        self.programming_language_id = programming_language_id
        self.app_id = app_id
        self.navigation_id = navigation_id
        self.document_id = document_id

    def __repr__(self):
        return '<Programming Code %r>' % self.handle