from container import app
from flask import Blueprint, render_template, abort, Flask
from jinja2 import TemplateNotFound

main = Blueprint('main', __name__)

@main.route('/')
def index():
    return render_template('main/index.html')