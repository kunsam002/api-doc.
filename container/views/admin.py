from container import app
from flask import Blueprint, render_template, abort, Flask
from jinja2 import TemplateNotFound
from container.forms import *

admin = Blueprint('admin', __name__)

@admin.route('/')
def index():
	return render_template('admin/navigation.html')

@admin.route('/app')
def doc():
	return render_template('admin/app.html')

@admin.route('/appform')
def appform():
	return render_template('admin/appform.html')
