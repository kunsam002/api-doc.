from setup import create_app, initialize_blueprints
from flask.ext.migrate import Migrate, MigrateCommand
from flask.ext.script import Manager
from config import Config
import os

app = create_app('documentation', Config)

migrate = Migrate(app, app.db)
manager = Manager(app)
manager.add_command('db', MigrateCommand)

@manager.command
def run_frontend():
	"""
		Frontend Application
	"""

	from container.views.main import main
	
	initialize_blueprints(app, main)

	port = int(os.environ.get('PORT', 5000))
	app.run(host='0.0.0', port=port, debug=True)

@manager.command
def run_backend():
	"""
		Backend Application
	"""
	with app.app_context():

		from container.views.admin import admin

		initialize_blueprints(app, admin)

		port = int(os.environ.get('PORT', 5001))
		app.run(host='0.0.0', port=port, debug=True)

if __name__ == '__main__':
    manager.run()